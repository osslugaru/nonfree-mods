# Tannim's mini campaign

Hey guys, with the help of Lotus, who organized this into a campaign, I have
made an 11 map run for Lugaru. There are mazes and relatively exotic maps.

I wanted to try something more unusual than the standard ones. I have seen
some good ones from other people. Hopefully these stand up too :)

All the enemies are Chibi. You will fit into this campaign better if you type

`proportion 2 1 1 0.2` at the start of every map.

Evil Twin is an unusual level. It is second to last though really it should
have been a final map. The rabbit is super powered and there is a high
probability that you WILL shoot through the walls when he hits you. You can get
back up into the tunnel with him, but it's usually easier to start over. To get
back up with him climb up the hill until you walk through the floor. If you
find it too frustrating, cheat and pop his head.

MultiLevel, the final map, may run very slowly. If so, sorry, those fires just
slow things down.
