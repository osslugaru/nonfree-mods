# Lugaru: Reluctant Assassin

After a whole three days in development, I give you...

In this Campaign we will follow Vincent Hirem, an ex-assassin, on his journey
through a rebellion against an oppressive Imperialist government. When fate
meets with his closest friends, he reluctantly resumes his life as an assassin
and his new target is the Imperialist leader.

This was made in three days out of boredom while while waiting on work to be
done on another project.

Have fun!

--Lotus Wolf
