# Non-free mods for (OSS) Lugaru

The term "non-free" refers to the lack of a free license which guarantees that
the copyrighted material can be freely redistributed, modified and even sold
(such a license would typically be CC-BY-SA 4.0).

See the LICENSE.txt files in each mod's folder for details. Eventually the aim
would be to get in touch with the authors of all those mods to get them to
relicense the files for which they own the copyright, so that we can include
them out of the box in [OSS Lugaru](https://osslugaru.gitlab.io).

Those mods have been ported so that they work with the latest release from the
OSS Lugaru project.
