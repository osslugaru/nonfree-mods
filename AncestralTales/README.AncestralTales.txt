Lugaru: Ancestral Tales
=======================

Lugaru: Ancestral Tales is an alternative campaign for Lugaru, split into two
campaign files to workaround the engine limit of 50 levels (the full campaign
has 60 levels).

Story
-----

"Long ago, in our grandmothers' grandmothers' time, before the tribes could
speak..."

Even as Turner seeks revenge for the killing of his family, an ancient and
deadly danger is uncovered. All but forgotten, it threatens the future of all
the races...

But some remember the old tales, and a new generation will rise to join the
fight.

Installation
------------

Copy the files of the Campaigns, Maps, Models and Textures folder in the
respective folders of your Lugaru installation. Some files will override
upstream Lugaru files (e.g. new weapon models, or some textures), so do not
hesitate to make a backup of your Data folder.

Known issues
------------

There are GLITCHES.
You'll see 'em.

1: Right in the VERY FIRST LEVEL! Your hero MAY load without any skin at all,
looking like a plastic FoxBot.

DO NOT PANIC!

It's a fox; you'll see plenty of her for the next couple of hours.
The map took a REALLY long time to build, and I just ain't going to redo it. 5
months is enough.

Anyone who can look at the code and repair it, my eternal thanks, you're doing
your community's brains a big service. =)

2: Sometimes a character can get knocked, or jump, through intersections in
walls or rocks. If they get trapped, you might have to explode their head to
finish the level. (play in debug mode, the "alt+i" key does it)

If you get trapped, you'll have to quit the level and start over again.

3: A couple of the dialogues load the characters weirdly at random times. If you
care about how the conversations look, you'll have to quit the level and replay
it.

Thanks
------

Groveller for the brilliant size workaround, probably took him 1 second to think
of it.

Lotus Wolf and Stromboli for the beautiful armor and weapons and Staffshock for
the fox skin, I KNOW those took hours and hours.

Count Roland and Lotus again for tech advice, Untadaike and Hazel-roo and
Skraeling for encouragement.

Of course thanks to David, who wrote the whole game, for chrissakes. Wow.
Thanks to John at Wolfire for his help, everyone is so nice there.

Anyone I've forgotten, thanks, and thanks all who put up with my noobish
questions.

And last and most, thanks to my extremely patient wife, the mighty T-'effin'-T
for her extreme patience, story support, and smooches.

Making of
---------

Hi all,
Thanks for playin'.

A few notes:

    I built this campaign on a "State of the 1997* Art OS X 10.4.11 PPC G4 iMac
with 640 MB SDRAM Memory" FREE(!) computer...

... so I couldn't do all I wanted due to horrible frame-rate droppage.
I was going to run 312 opponents on every fight level, or at least the 8 you're
allowed.

But my story had to have some buildings, too, and when I built them
--especially the interiors-- I had to cut 'em down to 6, or 3 or 4, even. :(

You'll know the levels when you see 'em. They're the elaborate maze and onward,
and the tunnels. The floors, ceilings & intersections really suck up the memory.
Skybox is already off where it's not needed in these levels, btw.

SO: If you need more meanies to fight in claustrophobic rooms, (and I know I
do), and you computer is more powerful than mine, (which it is, believe me),
feel free to populate 'em up to the maximum 8, resave the map, and kick some
Rabbit tail.

The levels are warren, greathall, tales32, tunnelin, descent.

(to keep in line with the character design, open console, sizenear 'em up to 1.4
or 1.5.
    --you'll see what I mean. Re-tinting their clothes to more manly shades
might be good, too...  try typing: noclothesnear, (enter), tint 0.4 0.4 0.4
(enter) then type: clothesnear Shirt (enter) clothesnear Pants, etc.)

If your kit is even slower than mine (unlikely, but possible) and you experience
visual lag, you might try setting viewdistance to 0.5 or so.

I've found that killing the first person you come across really helps smooth the
day along-- just like in real life!

Just kidding.

heh.

esplode ther hed.

heh,heh,heh.

   About the Oakey character: he's meant to be that slow! Hee, hee!
Actually, he was meant to be a big, burly guy with a few extra pounds on him--
think defensive lineman in American football-- really big, really strong, fat,
slower than the running backs, but way quicker and more agile than they look.

   However, due to a limitation in the game design, changes to size and
proportion can't be saved for your (the player's) character! He (or she) always
returns to standard "Turner" size and shape when you reload the level. :/

Strength, speed, armor, etc. do reload, so the fighting is as intended. =D

Size was easy to work around-- you'll see-- but the proportion thing stumped me.

   For amusement, you might try playing him the way I envisioned him. Open
console and type in: proportion 1.0 1.3 1.5 1.3. (or proportionnear 1.0 1.3 1.5
1.3, if he's not you) I think it adds to the realism and story if your guy LOOKS
fat, slow and strong... makes it doubly satisfying when you do land a
pile-driving punch upside the head of a nasty, quick moving wolf. BAM!



*i made this date up. i think it was really 1983. XD
