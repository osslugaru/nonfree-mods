Campaign Levels: 31

Level 1:
Name: tales2_01
Description: Crossing_the_Desert
ChooseNext: 1
NumNext: 1
NextLevel: 2
LocationX: 267
LocationY: 249

Level 2:
Name: tales2_02
Description: Clearwater_Ranch
ChooseNext: 0
NumNext: 1
NextLevel: 3
LocationX: 263
LocationY: 241

Level 3:
Name: tales2_03
Description: Sand_and_Thorns
ChooseNext: 0
NumNext: 1
NextLevel: 4
LocationX: 263
LocationY: 241

Level 4:
Name: tales2_04
Description: Birdpen1
ChooseNext: 0
NumNext: 1
NextLevel: 5
LocationX: 263
LocationY: 241

Level 5:
Name: tales2_05
Description: Birdpen2
ChooseNext: 0
NumNext: 1
NextLevel: 6
LocationX: 263
LocationY: 241

Level 6:
Name: tales2_06
Description: Birdpen3
ChooseNext: 0
NumNext: 1
NextLevel: 7
LocationX: 263
LocationY: 241

Level 7:
Name: tales2_07
Description: Pact
ChooseNext: 1
NumNext: 1
NextLevel: 8
LocationX: 263
LocationY: 241

Level 8:
Name: tales2_08
Description: The_Mountain
ChooseNext: 0
NumNext: 1
NextLevel: 9
LocationX: 286
LocationY: 228

Level 9:
Name: tales2_09
Description: The_Way_In
ChooseNext: 1
NumNext: 1
NextLevel: 10
LocationX: 286
LocationY: 228

Level 10:
Name: tales2_10
Description: Rose
ChooseNext: 0
NumNext: 1
NextLevel: 11
LocationX: 350
LocationY: 231

Level 11:
Name: tales2_11
Description: Palace_Roof
ChooseNext: 1
NumNext: 1
NextLevel: 12
LocationX: 350
LocationY: 231

Level 12:
Name: tales2_12
Description: Under_Ground
ChooseNext: 0
NumNext: 1
NextLevel: 13
LocationX: 286
LocationY: 228

Level 13:
Name: tales2_13
Description: Platform
ChooseNext: 0
NumNext: 1
NextLevel: 14
LocationX: 286
LocationY: 228

Level 14:
Name: tales2_14
Description: Balance
ChooseNext: 1
NumNext: 1
NextLevel: 15
LocationX: 286
LocationY: 228

Level 15:
Name: tales2_15
Description: Rose
ChooseNext: 0
NumNext: 1
NextLevel: 16
LocationX: 350
LocationY: 231

Level 16:
Name: tales2_16
Description: Turner_on_the_Roof
ChooseNext: 0
NumNext: 1
NextLevel: 17
LocationX: 350
LocationY: 231

Level 17:
Name: tales2_17
Description: Alder
ChooseNext: 0
NumNext: 1
NextLevel: 18
LocationX: 350
LocationY: 231

Level 18:
Name: tales2_18
Description: The_Lower_Hall
ChooseNext: 0
NumNext: 1
NextLevel: 19
LocationX: 350
LocationY: 231

Level 19:
Name: tales2_19
Description: Laurel
ChooseNext: 0
NumNext: 1
NextLevel: 20
LocationX: 350
LocationY: 231

Level 20:
Name: tales2_20
Description: Lily
ChooseNext: 0
NumNext: 1
NextLevel: 21
LocationX: 350
LocationY: 231

Level 21:
Name: tales2_21
Description: Rose_and_Turner
ChooseNext: 1
NumNext: 1
NextLevel: 22
LocationX: 350
LocationY: 231

Level 22:
Name: tales2_22
Description: Descent
ChooseNext: 1
NumNext: 1
NextLevel: 23
LocationX: 286
LocationY: 228

Level 23:
Name: tales2_23
Description: The_King's_Guards
ChooseNext: 0
NumNext: 1
NextLevel: 24
LocationX: 286
LocationY: 228

Level 24:
Name: tales2_24
Description: Up_the_Mountain
ChooseNext: 1
NumNext: 1
NextLevel: 25
LocationX: 286
LocationY: 228

Level 25:
Name: tales2_25
Description: The_Cavern_Floor
ChooseNext: 1
NumNext: 1
NextLevel: 26
LocationX: 286
LocationY: 228

Level 26:
Name: tales2_26
Description: Oakey's_Morning
ChooseNext: 1
NumNext: 1
NextLevel: 27
LocationX: 286
LocationY: 228

Level 27:
Name: tales2_27
Description: The_Conquerer_Tribe
ChooseNext: 0
NumNext: 1
NextLevel: 28
LocationX: 286
LocationY: 228

Level 28:
Name: tales2_28
Description: Ancestors
ChooseNext: 0
NumNext: 1
NextLevel: 29
LocationX: 286
LocationY: 228

Level 29:
Name: tales2_29
Description: The_Colonel
ChooseNext: 1
NumNext: 1
NextLevel: 30
LocationX: 350
LocationY: 231

Level 30:
Name: tales2_30
Description: Sunrise
ChooseNext: 1
NumNext: 1
NextLevel: 31
LocationX: 350
LocationY: 231

Level 31:
Name: tales2_31
Description: Epilogue
ChooseNext: 1
NumNext: 0
LocationX: 267
LocationY: 249
